-- Database creation
CREATE DATABASE ECE656_PROJECT;

-- Table creation

------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------
-- First we tried creating a table called fima to load in all of the data as per specified in the FEMA website
-- However, the data was missing and being parsed as empty strings, so we were getting errors and had to create a new
-- table called fimaTest with all fields as char and varchar to correctly parse and import the data, while replacing
-- empty strings to null values for missing data. See commented out code for fima at the end of this file. 
------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------

-- To import CSV data we had to configure some options on mysql server and mysql workbench as follows:

-- set global local_infile=true;
-- needed to go into edit connection on workbench on the given connection 
-- then needed to go into the current database and add "OPT_LOCAL_INFILE=1" under Others section
-- show global variables like 'local_infile';

-- Command to load CSV data into fima below:

-- LOAD DATA LOCAL INFILE '/home/user/Desktop/ECE656/ece656_project_group2/' INTO TABLE fima FIELDS TERMINATED BY ',' LINES TERMINATED BY '\n' IGNORE 1 LINES;

------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------

CREATE TABLE fimaTest(
    agricultureStructureIndicator varchar(25), 
    baseFloodElevation varchar(25), 
    basementEnclosureCrawlspace varchar(25), 
    censusTract varchar(25),
    cancellationDate varchar(25),
    condominiumIndicator varchar(25),
    construction varchar(25),
    countyCode varchar(5),
    crsClassCode varchar(25),
    deductibleAmountInBuildingCoverage varchar(25),
    deductibleAmountInContentsCoverage varchar(25),
    elevationBuildingIndicator varchar(25),
    elevationCertificateIndicator varchar(25),
    elevationDifference varchar(25),
    federalPolicyFee varchar(25),
    floodZone	varchar(25),
    hfiaaSurcharge	 varchar(25),
    houseOfWorshipIndicator varchar(25),
    latitude varchar(25),
    longitude varchar(25),
    locationOfContents varchar(25),
    lowestAdjacentGrade	varchar(25),
    lowestFloorElevation varchar(25),
    nonProfitIndicator	varchar(25),
    numberOfFloorsInTheInsuredBuilding	char(1),
    obstructionType	varchar(25),
    occupancyType	char(1), 
    originalConstructionDate varchar(25),
    originalNBDate	varchar(25),
    policyCost	varchar(25),
    policyCount	varchar(25), 
    policyEffectiveDate	varchar(25),
    policyTerminationDate	varchar(25),
    policyTermIndicator	char(1), 
    postFIRMConstructionIndicator	varchar(25), 
    primaryResidenceIndicator	varchar(25), 
    propertyState	varchar(25),
    reportedZipCode	varchar(25),
    rateMethod	varchar(25),
    regularEmergencyProgramIndicator varchar(25),
    reportedCity	varchar(25), 
    smallBusinessIndicatorBuilding	varchar(25),
    totalBuildingInsuranceCoverage	varchar(25),
    totalContentsInsuranceCoverage	varchar(25),
    totalInsurancePremiumOfThePolicy	varchar(25), 
    id varchar(25),
    primary key (id)
);

-- Loading of CSV data onto fimaTest and replacing '' for NULL in every field:

LOAD DATA LOCAL INFILE '/home/user/Desktop/ECE656/ece656_project_group2/fpolicies.csv' INTO TABLE fimaTest FIELDS TERMINATED BY ',' LINES TERMINATED BY '\n' IGNORE 1 LINES
(@agricultureStructureIndicator, @baseFloodElevation, @basementEnclosureCrawlspace, @censusTract ,
    @cancellationDate,@condominiumIndicator,@construction,@countyCode,@crsClassCode , @deductibleAmountInBuildingCoverage,
    @deductibleAmountInContentsCoverage,@elevationBuildingIndicator, @elevationCertificateIndicator , @elevationDifference,
    @federalPolicyFee,@floodZone,@hfiaaSurcharge,@houseOfWorshipIndicator,@latitude ,@longitude ,@locationOfContents,
    @lowestAdjacentGrade,@lowestFloorElevation,@nonProfitIndicator,@numberOfFloorsInTheInsuredBuilding,
    @obstructionType	,@occupancyType, @originalConstructionDate,@originalNBDate,policyCost,policyCount, 
    @policyEffectiveDate	,@policyTerminationDate,@policyTermIndicator, @postFIRMConstructionIndicator, 
    @primaryResidenceIndicator, @propertyState,@reportedZipCode,@rateMethod,@regularEmergencyProgramIndicator,@reportedCity, 
    @smallBusinessIndicatorBuilding,@totalBuildingInsuranceCoverage,@totalContentsInsuranceCoverage,@totalInsurancePremiumOfThePolicy, 
    id )
    SET agricultureStructureIndicator = IF(@agricultureStructureIndicator = '', NULL, @agricultureStructureIndicator),baseFloodElevation = IF(@baseFloodElevation = '', NULL, @baseFloodElevation),
    basementEnclosureCrawlspace = IF(@basementEnclosureCrawlspace = '', NULL, @basementEnclosureCrawlspace),
    censusTract = IF(@censusTract = '', NULL, @censusTract),
    cancellationDate = IF(@cancellationDate = '', NULL, @cancellationDate), condominiumIndicator = IF(@condominiumIndicator = '', NULL, @condominiumIndicator),
    ,construction = IF(@construction = '', NULL, @construction),countyCode = IF(@countyCode = '', NULL, @countyCode), crsClassCode = IF(@crsClassCode = '', NULL, @crsClassCode), deductibleAmountInBuildingCoverage = IF(@deductibleAmountInBuildingCoverage ='', NULL, @deductibleAmountInBuildingCoverage),
    deductibleAmountInContentsCoverage = IF(@deductibleAmountInContentsCoverage = '',NULL, @deductibleAmountInContentsCoverage),elevationBuildingIndicator = IF(@elevationBuildingIndicator ='', NULL, @elevationBuildingIndicator) ,
    elevationCertificateIndicator = IF(@elevationCertificateIndicator ='', NULL, @elevationCertificateIndicator), elevationDifference = IF(@elevationDifference ='', NULL, @elevationDifference),
    federalPolicyFee = IF(@federalPolicyFee ='', NULL, @federalPolicyFee), floodZone = IF(@floodZone ='', NULL, @floodZone), hfiaaSurcharge = IF(@hfiaaSurcharge ='', NULL, @hfiaaSurcharge),
    houseOfWorshipIndicator = IF(@houseOfWorshipIndicator ='', NULL, @houseOfWorshipIndicator), latitude = IF(@latitude ='', NULL, @latitude),
    longitude = IF(@longitude ='', NULL, @longitude), locationOfContents = IF(@locationOfContents ='', NULL, @locationOfContents),lowestAdjacentGrade = IF(@lowestAdjacentGrade ='', NULL, @lowestAdjacentGrade),
    lowestFloorElevation = IF(@lowestFloorElevation ='', NULL, @lowestFloorElevation), nonProfitIndicator = IF(@nonProfitIndicator ='', NULL, @nonProfitIndicator),
    numberOfFloorsInTheInsuredBuilding = IF(@numberOfFloorsInTheInsuredBuilding ='', NULL, @numberOfFloorsInTheInsuredBuilding),obstructionType = IF(@obstructionType ='', NULL, @obstructionType),
    occupancyType = IF(@occupancyType ='', NULL, @occupancyType), originalConstructionDate = IF(@originalConstructionDate ='', NULL, @originalConstructionDate),
    originalNBDate = IF(@originalNBDate ='', NULL, @originalNBDate),policyCost = IF(@policyCost ='', NULL, @policyCost), policyCount = IF(@policyCount ='', NULL, @policyCount),
    policyEffectiveDate = IF(@policyEffectiveDate ='', NULL, @policyEffectiveDate),  policyTerminationDate = IF(@policyTerminationDate ='', NULL, @policyTerminationDate), policyTermIndicator = IF(@policyTermIndicator ='', NULL, @policyTermIndicator),
    postFIRMConstructionIndicator = IF(@postFIRMConstructionIndicator ='', NULL, @postFIRMConstructionIndicator), primaryResidenceIndicator = IF(@primaryResidenceIndicator ='', NULL, @primaryResidenceIndicator),
    propertyState = IF(@propertyState ='', NULL, @propertyState), reportedZipCode = IF(@reportedZipCode ='', NULL, @reportedZipCode), rateMethod = IF(@rateMethod ='', NULL, @rateMethod),
    regularEmergencyProgramIndicator = IF(@regularEmergencyProgramIndicator ='', NULL, @regularEmergencyProgramIndicator), reportedCity = IF(@reportedCity ='', NULL, @reportedCity),
    smallBusinessIndicatorBuilding = IF(@smallBusinessIndicatorBuilding ='', NULL, @smallBusinessIndicatorBuilding),  totalBuildingInsuranceCoverage = IF(@totalBuildingInsuranceCoverage ='', NULL, @totalBuildingInsuranceCoverage),
    totalContentsInsuranceCoverage = IF(@totalContentsInsuranceCoverage ='', NULL, @totalContentsInsuranceCoverage), totalInsurancePremiumOfThePolicy = IF(@totalInsurancePremiumOfThePolicy ='', NULL, @totalInsurancePremiumOfThePolicy);

-- All CSV Data is correctly loaded up until this point

-- Then we create tables for the entities in the ER model, converting the model into a relationship schema:

-- We create auxiliary tables to map deductible amounts in contents and building coverage from FloodPolicy
CREATE TABLE dabcMapping(
    deductibleAmountInBuildingCoverage char(1),
    dabcMonetaryValue int,
    primary key (deductibleAmountInBuildingCoverage)
    -- add foreign key in FloodPolicy table
);

INSERT INTO dabcMapping (deductibleAmountInBuildingCoverage, dabcMonetaryValue) 
VALUES ('0',500),('1',1000),('2',2000), ('3',3000),('4',4000),('5',5000),('9',750),('A',10000),('B',150000),('C',20000),('D',25000),('E',50000),
('F',1250),('G',1500);

CREATE TABLE daccMapping(
    deductibleAmountInContentsCoverage char(1),
    daccMonetaryValue int,
    primary key (deductibleAmountInContentsCoverage)
    -- add foreign key in FloodPolicy table
);

INSERT INTO daccMapping (deductibleAmountInContentsCoverage, daccMonetaryValue) 
VALUES ('0',500),('1',1000),('2',2000), ('3',3000),('4',4000),('5',5000),('9',750),('A',10000),('B',150000),('C',20000),('D',25000),('E',50000),
('F',1250),('G',1500);

-------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------
-- Create FloodPolicy Table

CREATE TABLE FloodPolicy(
    policyID char(25),
    cancellationDate date,
    cancellationTime time,
    crsClassCode int,
    deductibleAmountInBuildingCoverage char(1),
    deductibleAmountInContentsCoverage char(1),
    elevationCertificateIndicator int,
    federalPolicyFee int,
    hfiaaSurcharge int,
    houseOfWorshipIndicator int,
    originalNBDate date,
    originalNBTime time,
    policyCost int,
    policyCount int,
    policyEffectiveDate date,
    policyEffectiveTime time,
    policyTerminationDate date,
    policyTerminationTime time,
    policyTermIndicator int,
    rateMethod char(1),
    regularEmergencyProgramIndicator char(1),
    totalBuildingInsuranceCoverage int,
    totalContentsInsuranceCoverage int,
    totalInsurancePremiumOfThePolicy int,
    primary key (policyID),
    foreign key (deductibleAmountInBuildingCoverage) references dabcMapping(deductibleAmountInBuildingCoverage),
    foreign key (deductibleAmountInContentsCoverage) references daccMapping(deductibleAmountInContentsCoverage),
    CONSTRAINT CHK_policyTermIndicator check (policyTermIndicator = 1 OR policyTermIndicator = 9 OR policyTermIndicator = 3),
    CONSTRAINT CHK_negativeDatesFloodPolicy CHECK(DATEDIFF(policyTerminationDate, policyEffectiveDate) >= 0)


); -- end of flood policy table

--select DATEDIFF(policyTerminationDate,policyEffectiveDate), policyTerminationDate, policyEffectiveDate, policyTermIndicator from FloodPolicy where ((DATEDIFF(policyTerminationDate,policyEffectiveDate) between 367 AND 1095) and policyTermIndicator=1) ;


-- Insert data into FloodPolicy from fimaTest

INSERT INTO FloodPolicy (policyID,cancellationDate,cancellationTime,crsClassCode,deductibleAmountInBuildingCoverage,deductibleAmountInContentsCoverage,elevationCertificateIndicator,federalPolicyFee,
hfiaaSurcharge,houseOfWorshipIndicator,originalNBDate,originalNBTime,policyCost,policyCount,policyEffectiveDate,policyEffectiveTime,policyTerminationDate,policyTerminationTime,rateMethod,regularEmergencyProgramIndicator,
totalBuildingInsuranceCoverage,totalContentsInsuranceCoverage,totalInsurancePremiumOfThePolicy, policyTermIndicator) 
SELECT id,regexp_substr(cancellationDate, '[^T]+'), substring(cancellationDate, 12,8), crsClassCode,deductibleAmountInBuildingCoverage,deductibleAmountInContentsCoverage,elevationCertificateIndicator,federalPolicyFee,hfiaaSurcharge,houseOfWorshipIndicator,
regexp_substr(originalNBDate, '[^T]+'), substring(originalNBDate, 12,8),
policyCost,policyCount,regexp_substr(policyEffectiveDate, '[^T]+'), substring(policyEffectiveDate, 12,8), regexp_substr(policyTerminationDate, '[^T]+'),substring(policyTerminationDate, 12,8),rateMethod,regularEmergencyProgramIndicator,totalBuildingInsuranceCoverage,
totalContentsInsuranceCoverage,totalInsurancePremiumOfThePolicy, policyTermIndicator
FROM fimaTest;
-- Create Location Table

CREATE TABLE Location(
    policyID char(25),
    locationID int NOT NULL AUTO_INCREMENT,
    censusTract decimal(11) UNSIGNED ZEROFILL,
    countyCode decimal(5),
    latitude decimal(4,1),
    longitude decimal(4,1),
    propertyState char(2),
    reportedZipCode decimal(5),
    reportedCity varchar(36),
    primary key (locationID, policyID),
    foreign key (policyID) references FloodPolicy(policyID),
    index (propertyState)
);

-- Insert data into Location from fimaTest

INSERT INTO Location (policyID ,censusTract ,countyCode ,latitude ,longitude ,propertyState ,reportedZipCode ,reportedCity) 
SELECT id ,censusTract ,substring(countyCode, 1,5) ,latitude ,longitude ,propertyState ,substring(reportedZipCode, 1,5) ,reportedCity 
FROM fimaTest LIMIT;

-- Create FloodLocation Table

CREATE TABLE  FloodLocation(
    policyID char(25),
    locationID int NOT NULL AUTO_INCREMENT,
    baseFloodElevation int,
    floodZone varchar(5),
    primary key (policyID,locationID),
    foreign key (policyID) references FloodPolicy(policyID),
    -- foreign key below not specified in ER diagram because FloodLocation is a partial specialization of Location
    foreign key (locationID,policyID) references Location(locationID,policyID),
    index (baseFloodElevation)
);

-- Insert data into FloodLocation from fimaTest

INSERT INTO FloodLocation(policyID, baseFloodElevation, floodZone)
SELECT id, baseFloodElevation, floodZone from fimaTest;


-- Create Building Table

CREATE TABLE Building(
    policyID char(25),
    buildingID int NOT NULL AUTO_INCREMENT,
    agricultureStructureIndicator boolean,
    basementEnclosureCrawlspace int,
    condominiumIndicator char(1),
    construction boolean,
    elevationBuildingIndicator boolean,
    elevationDifference int,
    locationOfContents int,
    lowestAdjacentGrade int,
    lowestFloorElevation decimal(6,2),
    numberOfFloorsInTheInsuredBuilding int,
    obstructionType int,
    occupancyType int,
    postFIRMConstructionIndicator boolean,
    originalConstructionDate date,
    originalConstructionTime time,
    primary key (buildingID,policyID),
    foreign key (policyID) references FloodPolicy(policyID),
    index(lowestFloorElevation)
);
--creating cross tuple check between two tables was not feasible due to the current database design that we chose hence, we needed to only add it to the client to reduce human error. 
-- (this is for attribute originalNB date located in floodpolicy cross tuple checking with originalconstructiondate in building)
-- Insert data into Building from fimaTest

INSERT INTO Building (policyID, agricultureStructureIndicator, basementEnclosureCrawlspace, condominiumIndicator, construction, elevationBuildingIndicator, elevationDifference,
locationOfContents, lowestAdjacentGrade, lowestFloorElevation, numberOfFloorsInTheInsuredBuilding ,obstructionType, occupancyType, postFIRMConstructionIndicator, originalConstructionDate, originalConstructionTime)
SELECT id, agricultureStructureIndicator, basementEnclosureCrawlspace, condominiumIndicator, construction, elevationBuildingIndicator, elevationDifference,
locationOfContents, lowestAdjacentGrade, lowestFloorElevation, numberOfFloorsInTheInsuredBuilding, obstructionType, occupancyType, postFIRMConstructionIndicator, regexp_substr(originalConstructionDate, '[^T]+'), substring(originalConstructionDate, 12,8)
FROM fimaTest;

-- Create Owner Table

CREATE TABLE Owner(
    policyID char(25),
    buildingID int NOT NULL AUTO_INCREMENT,
    primaryResidenceIndicator boolean, 
    smallBusinessIndicatorBuilding boolean,
    nonProfitIndicator boolean,
    primary key (buildingID,policyID,primaryResidenceIndicator,smallBusinessIndicatorBuilding),
    foreign key (buildingID,policyID) references Building(buildingID,policyID),
    foreign key (policyID) references FloodPolicy(policyID)
);

-- Insert data into Owner from fimaTest

INSERT INTO Owner (policyID, primaryResidenceIndicator, smallBusinessIndicatorBuilding, nonProfitIndicator)
SELECT id, primaryResidenceIndicator, smallBusinessIndicatorBuilding, nonProfitIndicator FROM fimaTest;

-- Add a missing foreign key of Owner referencing FloodPolicy
-- ALTER TABLE Owner ADD FOREIGN KEY (policyID) references FloodPolicy(policyID);
-- ALTER TABLE Owner DROP FOREIGN KEY Owner_ibfk_1;
-------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------

-- Reference code for fima table creation: 

-- CREATE TABLE fima(
--     agricultureStructureIndicator int, https://prod.liveshare.vsengsaas.visualstudio.com/join?5B608643F6EC2D76C375310BFF55F8B5009B
--     construction int,
--     countyCode varchar(5),
--     crsClassCode varchar(25),
--     deductibleAmountInBuildingCoverage varchar(25),
--     deductibleAmountInContentsCoverage varchar(25),
--     elevationBuildingIndicator int,
--     elevationCertificateIndicator varchar(25),
--     elevationDifference varchar(25),
--     federalPolicyFee int,
--     floodZone	varchar(25),
--     hfiaaSurcharge	 int,
--     houseOfWorshipIndicator int,
--     latitude varchar(25),
--     longitude varchar(25),
--     locationOfContents char(1),
--     lowestAdjacentGrade	int,
--     lowestFloorElevation varchar(25),
--     nonProfitIndicator	int,
--     numberOfFloorsInTheInsuredBuilding	char(1),
--     obstructionType	varchar(25),
--     occupancyType	char(1), 
--     originalConstructionDate varchar(25),
--     originalNBDate	varchar(25),
--     policyCost	int,
--     policyCount	int, 
--     policyEffectiveDate	varchar(25),
--     policyTerminationDate	varchar(25),
--     policyTermIndicator	char(1), 
--     postFIRMConstructionIndicator	int, 
--     primaryResidenceIndicator	int, 
--     propertyState	varchar(25),
--     reportedZipCode	varchar(25),
--     rateMethod	varchar(25),
--     regularEmergencyProgramIndicator varchar(25),
--     reportedCity	varchar(25), 
--     smallBusinessIndicatorBuilding	boolean,
--     totalBuildingInsuranceCoverage	varchar(25),
--     totalContentsInsuranceCoverage	varchar(25),
--     totalInsurancePremiumOfThePolicy	varchar(25), 
--     id varchar(25),
--     primary key (id)
-- );

	-- CONSTRAINT CHK_policyTermIndicatorOne check(((DATEDIFF(policyTerminationDate,policyEffectiveDate) <=1096)) 
	-- CONSTRAINT CHK_policyTermIndicatorOne check(((DATEDIFF(policyTerminationDate,policyEffectiveDate) <=1096) AND (policyTermIndicator=1 OR policyTermIndicator=2 OR policyTermIndicator=NULL OR policyTermIndicator=0)) 

