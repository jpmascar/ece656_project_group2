import { con } from "../app.js";

export const getEstimate = function (req, res) {

  //retrieve data from getEstimate UI form from Owner
  let data = req.body;

  let OwnerWithString = "WITH OwnerInfo AS (SELECT * FROM Owner WHERE ";
  let BuildingWithString = ", BuildingInfo AS (SELECT * FROM Building WHERE ";
  let LocationWithString = ", LocationInfo AS (SELECT * FROM Location WHERE ";
  let FloodInformationWithString =
    ", FLInfo AS (SELECT * FROM FloodLocation WHERE ";

  let OwnerW = true;
  let BuildingW = true;
  let LocationW = true;
  let FloodInformationW = true;
  for (const [key, value] of Object.entries(data)) {
    if (
      key === "primaryResidenceIndicator" ||
      key === "smallBusinessIndicatorBuilding" ||
      (key === "nonProfitIndicator" && !(value === ""))
    ) {
      if (OwnerW == true) {
        OwnerWithString += key + "=" + `'${value}'`;
        OwnerW = false;
      } else {
        OwnerWithString += " and " + key + "=" + `'${value}'`;
      }
    } //end owner if

    if (
      (key === "agricultureStructureIndicator" ||
        key === "basementEnclosureCrawlspace" ||
        key === "condominiumIndicator" ||
        key === "construction" ||
        key === "elevationBuildingIndicator" ||
        key === "elevationDifference" ||
        key === "locationOfContents" ||
        key === "lowestAdjacentGrade" ||
        key === "lowestFloorElevation" ||
        key === "obstructionType" ||
        key === "occupancyType" ||
        key === "postFIRMConstructionIndicator" ||
        key === "originalConstructionDate" ||
        key === "originalConstructionTime" ||
        key === "numberOfFloorsInTheInsuredBuilding") &&
      !(value === "")
    ) {
      if (BuildingW == true) {
        BuildingWithString += key + "=" + `'${value}'`;
        BuildingW = false;
      } else {
        BuildingWithString += " and " + key + "=" + `'${value}'`;
      }
    } //end building if
    if (
      (key === "latitude" ||
        key === "longitude" ||
        key === "propertyState" ||
        key === "reportedZipCode" ||
        key === "reportedCity") &&
      !(value === "")
    ) {
      if (LocationW == true) {
        LocationWithString += key + "=" + `'${value}'`;
        LocationW = false;
      } else {
        LocationWithString += " and " + key + "=" + `'${value}'`;
      }
    } //end location if

    if (
      (key === "baseFloodElevation" || key === "floodZone") &&
      !(value === "")
    ) {
      if (FloodInformationW == true) {
        FloodInformationWithString += key + "=" + `'${value}'`;
        FloodInformationW = false;
      } else {
        FloodInformationWithString += " and " + key + "=" + `'${value}'`;
      }
    } //end of flood infomation if
  } //end for loop

  OwnerWithString += " LIMIT 5000)"; //adding closing bracket for with statement
  BuildingWithString += " LIMIT 5000)";
  LocationWithString += " LIMIT 5000)";
  FloodInformationWithString += " LIMIT 5000)";

  let WithStatementQuery = OwnerWithString;
  let AverageQuery =
    " SELECT AVG(totalBuildingInsuranceCoverage) , AVG(dabcMonetaryValue), AVG(daccMonetaryValue),AVG(federalPolicyFee),AVG(hfiaaSurcharge),AVG(totalContentsInsuranceCoverage),AVG(totalInsurancePremiumOfThePolicy) FROM "; //NOTE can just replace * with AVG(certain column we want) in order to get the average of each column
  let FloodPolicyJoinString =
    "(FloodPolicy AS fo INNER JOIN OwnerInfo as o ON fo.policyID = o.policyID";
  //perfoming dynamic string insertion depending on which attributes the user selected
  if (BuildingW == false) {
    WithStatementQuery += BuildingWithString;
    FloodPolicyJoinString +=
      " INNER JOIN BuildingInfo AS bld ON fo.policyID = bld.policyID";
  }
  if (LocationW == false) {
    WithStatementQuery += LocationWithString;
    FloodPolicyJoinString +=
      " INNER JOIN LocationInfo AS lo ON fo.policyID = lo.policyID";
  }
  if (FloodInformationW == false) {
    WithStatementQuery += FloodInformationWithString;
    FloodPolicyJoinString +=
      " INNER JOIN FLInfo AS fl ON fo.policyID = fl.policyID";
  }

  FloodPolicyJoinString +=
    " LEFT JOIN dabcMapping as dabc ON fo.deductibleAmountInBuildingCoverage = dabc.deductibleAmountInBuildingCoverage LEFT JOIN daccMapping as dacc ON fo.deductibleAmountInContentsCoverage = dacc.deductibleAmountInContentsCoverage)";

  let finalStringNew =
    WithStatementQuery + AverageQuery + FloodPolicyJoinString;
  //performing query

  console.log(finalStringNew)
  const result = con.query(finalStringNew, function (err, result, fields) {
    if (err) throw err;
    let resultArray = Object.values(JSON.parse(JSON.stringify(result)));
    console.log(resultArray);
    res.render("estimate.pug", { queryEstimate: resultArray });
  });
};
