import { con } from "../app.js";

export const updateRecord = function (req, res) {
  
  //retrieve policyID of record to be updated from route parameters
  let updateRecordPolicyID = req.params.id;

  //retrieve update data from UI input by FEMA Staff
  //Please note that every input field will be overriden
  let updateInfo = req.body;

  let stringUpdateOwnerNames = "UPDATE Owner ";
  let stringUpdateOwnerValues = "SET ";

  let stringUpdateBuildingNames = "UPDATE Building ";
  let stringUpdateBuildingValues = "SET ";

  let stringUdateLocationNames = "UPDATE Location ";
  let stringUpdateLocationValues = "SET ";

  let stringUpdateFloodInformationNames = "UPDATE FloodLocation ";
  let stringUpdateFloodInformationValues = "SET ";

  let stringUpdateFloodPolicyNames = "UPDATE FloodPolicy ";
  let stringUpdateFloodPolicyValues = "SET ";
  let ownerupdatefirst = true;
  let buildingupdatefirst = true;
  let locationupdatefirst = true;
  let floodinformationupdatefirst = true;
  let floodpolicyupdatefirst = true;

  for (const [key, value] of Object.entries(updateInfo)) {

    //owner query update

    if (
      key === "primaryResidenceIndicator" ||
      key === "smallBusinessIndicatorBuilding" ||
      key === "nonProfitIndicator"
    ) {
      if (ownerupdatefirst == true) {
        if (value === "") {
          stringUpdateOwnerValues += key + "=NULL ";
        } else {
          stringUpdateOwnerValues += key + "=" + `'${value}'`;
        }
        ownerupdatefirst = false;
      } else {
        if (value === "") {
          stringUpdateOwnerValues += "," + key + "=NULL ";
        } else {
          stringUpdateOwnerValues += "," + key + "=" + `'${value}'`;
        }
      }
    } //end owner if

    //building query update
    if (
      key === "agricultureStructureIndicator" ||
      key === "basementEnclosureCrawlspace" ||
      key === "condominiumIndicator" ||
      key === "construction" ||
      key === "elevationBuildingIndicator" ||
      key === "elevationDifference" ||
      key === "locationOfContents" ||
      key === "lowestAdjacentGrade" ||
      key === "lowestFloorElevation" ||
      key === "obstructionType" ||
      key === "occupancyType" ||
      key === "postFIRMConstructionIndicator" ||
      key === "originalConstructionDate" ||
      key === "originalConstructionTime" ||
      key === "numberOfFloorsInTheInsuredBuilding"
    ) {
      if (buildingupdatefirst == true) {
        if (value === "") {
          stringUpdateBuildingValues += key + "=NULL ";
        } else {
          stringUpdateBuildingValues += key + "=" + `'${value}'`;
        }
        buildingupdatefirst = false;
      } else {
        if (value === "") {
          stringUpdateBuildingValues += "," + key + "=NULL ";
        } else {
          stringUpdateBuildingValues += "," + key + "=" + `'${value}'`;
        }
      }
    } //end building if

    //location query update
    if (
      key === "censusTract" ||
      key === "countyCode" ||
      key === "latitude" ||
      key === "longitude" ||
      key === "propertyState" ||
      key === "reportedZipCode" ||
      key === "reportedCity"
    ) {
      if (locationupdatefirst == true) {
        if (value === "") {
          stringUpdateLocationValues += key + "=NULL ";
        } else {
          stringUpdateLocationValues += key + "=" + `'${value}'`;
        }
        locationupdatefirst = false;
      } else {
        if (value === "") {
          stringUpdateLocationValues += "," + key + "=NULL ";
        } else {
          stringUpdateLocationValues += "," + key + "=" + `'${value}'`;
        }
      }
    } //end location if

    //Flood information query update
    if (
      key === "policyID" ||
      key === "baseFloodElevation" ||
      key === "floodZone"
    ) {
      if (floodinformationupdatefirst == true) {
        if (value === "") {
          stringUpdateFloodInformationValues += key + "=NULL ";
        } else {
          stringUpdateFloodInformationValues += key + "=" + `'${value}'`;
        }
        floodinformationupdatefirst = false;
      } else {
        if (value === "") {
          stringUpdateFloodInformationValues += "," + key + "=NULL ";
        } else {
          stringUpdateFloodInformationValues += "," + key + "=" + `'${value}'`;
        }
      }
    } //end flood information if statement

    //flood policy query update
    if (
      key === "policyID" ||
      key === "cancellationDate" ||
      key === "cancellationTime" ||
      key === "crsClassCode" ||
      key === "deductibleAmountInBuildingCoverage" ||
      key === "deductibleAmountInContentsCoverage" ||
      key === "elevationCertificateIndicator" ||
      key === "federalPolicyFee" ||
      key === "hfiaaSurcharge" ||
      key === "houseOfWorshipIndicator" ||
      key === "originalNBDate" ||
      key === "originalNBTime" ||
      key === "policyCost" ||
      key === "policyCount" ||
      key === "policyEffectiveDate" ||
      key === "policyEffectiveTime" ||
      key === "policyTerminationDate" ||
      key === "policyTerminationTime" ||
      key === "policyTermIndicator" ||
      key === "rateMethod" ||
      key === "regularEmergencyProgramIndicator" ||
      key === "totalBuildingInsuranceCoverage" ||
      key === "totalContentsInsuranceCoverage" ||
      key === "totalInsurancePremiumOfThePolicy"
    ) {
      if (floodpolicyupdatefirst == true) {
        if (value === "") {
          stringUpdateFloodPolicyValues += key + "=NULL ";
        } else {
          stringUpdateFloodPolicyValues += key + "=" + `'${value}'`;
        }
        floodpolicyupdatefirst = false;
      } else {
        if (value === "") {
          stringUpdateFloodPolicyValues += "," + key + "=NULL ";
        } else {
          stringUpdateFloodPolicyValues += "," + key + "=" + `'${value}'`;
        }
      }
    } //end floodpolicy if statement
  } //end for loop
  //combining update statements with wherer statement
  let stringUpdateFinalOwner =
    stringUpdateOwnerNames +
    stringUpdateOwnerValues +
    " WHERE policyID = " +
    `'${updateRecordPolicyID}'`;
  let stringUpdateFinalBuilding =
    stringUpdateBuildingNames +
    stringUpdateBuildingValues +
    " WHERE policyID = " +
    `'${updateRecordPolicyID}'`;
  let stringUdateFinalLocation =
    stringUdateLocationNames +
    stringUpdateLocationValues +
    " WHERE policyID = " +
    `'${updateRecordPolicyID}'`;
  let stringUpdateFinalFloodInformation =
    stringUpdateFloodInformationNames +
    stringUpdateFloodInformationValues +
    " WHERE policyID = " +
    `'${updateRecordPolicyID}'`;
  let stringUpdateFinalFloodPolicy =
    stringUpdateFloodPolicyNames +
    stringUpdateFloodPolicyValues +
    " WHERE policyID = " +
    `'${updateRecordPolicyID}'`;

  let StringGetpolicyIDB =
    "WITH OwnerT as (SELECT * FROM Owner WHERE policyID =" +
    `'${updateRecordPolicyID}'` +
    "),";
  let StringGetpolicyID2B =
    "BuildingT as (SELECT * FROM Building WHERE policyID =" +
    `'${updateRecordPolicyID}'` +
    "),";
  let StringGetpolicyID3B =
    "LocationT as (SELECT * FROM Location WHERE policyID =" +
    ` '${updateRecordPolicyID}'` +
    "),";
  let StringGetpolicyID4B =
    "FloodInformationT as (SELECT * FROM FloodLocation WHERE policyID =" +
    `'${updateRecordPolicyID}'` +
    "),";
  let StringGetpolicyID5B =
    "FloodPolicyT as (SELECT * FROM FloodPolicy WHERE policyID =" +
    `'${updateRecordPolicyID}'` +
    ") SELECT * FROM (OwnerT JOIN BuildingT JOIN LocationT JOIN FloodInformationT JOIN FloodPolicyT)";

  let FStringGetPolicyIDBackup =
    StringGetpolicyIDB +
    StringGetpolicyID2B +
    StringGetpolicyID3B +
    StringGetpolicyID4B +
    StringGetpolicyID5B;

  //con query for selects to information for backtracking if needed
  //creating backtracking queries
  let restoreStringOwner = "UPDATE Owner SET ";
  let restoreStringBuilding = "UPDATE Building SET ";
  let restoreStringLocation = "UPDATE Location SET ";
  let restoreStringFloodInformation = "UPDATE FloodLocation SET ";
  let restoreStringFloodPolicy = "UPDATE FloodPolicy SET ";
  let ownerupdatefirst1 = true;
  let buildingupdatefirst1 = true;
  let locationupdatefirst1 = true;
  let floodinformationupdatefirst1 = true;
  let floodpolicyupdatefirst1 = true;

  //NOTE: NEED TO FIX THE ASYNC CALLS HERE
  //IF ALL ELSE FAILS TO BE FIXED, NEED TO STATE IN VIDEO AS TO WHY WE MADE ALL THIS IN ONE CON.QUERY
  con.query(FStringGetPolicyIDBackup, function (err, result, fields) {
    if (err) callback(err);
    //console.log('executing query outside for loop')
    for (const [key, value] of Object.entries(result[0])) {
      // console.log(key)
      if (
        key === "primaryResidenceIndicator" ||
        key === "smallBusinessIndicatorBuilding" ||
        key === "nonProfitIndicator"
      ) {
        //owner if here
        if (ownerupdatefirst1 == true) {
          restoreStringOwner += key.toString() + "=" + `'${value}'`;
          ownerupdatefirst1 = false;
        } else {
          restoreStringOwner += "," + key.toString() + "=" + `'${value}'`;
        }
      }

      //building portion
      if (
        key === "agricultureStructureIndicator" ||
        key === "basementEnclosureCrawlspace" ||
        key === "condominiumIndicator" ||
        key === "construction" ||
        key === "elevationBuildingIndicator" ||
        key === "elevationDifference" ||
        key === "locationOfContents" ||
        key === "lowestAdjacentGrade" ||
        key === "lowestFloorElevation" ||
        key === "obstructionType" ||
        key === "occupancyType" ||
        key === "postFIRMConstructionIndicator" ||
        key === "originalConstructionDate" ||
        key === "originalConstructionTime" ||
        key === "numberOfFloorsInTheInsuredBuilding"
      ) {
        if (buildingupdatefirst1 == true) {
          restoreStringBuilding += key.toString() + "=" + `'${value}'`;
          buildingupdatefirst1 = false;
        } else {
          restoreStringBuilding += "," + key.toString() + "=" + `'${value}'`;
        }
      }

      //performing location
      if (
        key === "censusTract" ||
        key === "countyCode" ||
        key === "latitude" ||
        key === "longitude" ||
        key === "propertyState" ||
        key === "reportedZipCode" ||
        key === "reportedCity"
      ) {
        if (locationupdatefirst1 == true) {
          restoreStringLocation += key + "=" + `'${value}'`;
          locationupdatefirst1 = false;
        } else {
          restoreStringLocation += "," + key + "=" + `'${value}'`;
        }
      }

      //flood information part for backup
      if (
        key === "policyID" ||
        key === "baseFloodElevation" ||
        key === "floodZone"
      ) {
        if (floodinformationupdatefirst1 == true) {
          restoreStringFloodInformation += key + "=" + `'${value}'`;
          floodinformationupdatefirst1 = false;
        } else {
          restoreStringFloodInformation += "," + key + "=" + `'${value}'`;
        }
      }

      //performing flood policy
      if (
        key === "policyID" ||
        key === "cancellationDate" ||
        key === "cancellationTime" ||
        key === "crsClassCode" ||
        key === "deductibleAmountInBuildingCoverage" ||
        key === "deductibleAmountInContentsCoverage" ||
        key === "elevationCertificateIndicator" ||
        key === "federalPolicyFee" ||
        key === "hfiaaSurcharge" ||
        key === "houseOfWorshipIndicator" ||
        key === "originalNBDate" ||
        key === "originalNBTime" ||
        key === "policyCost" ||
        key === "policyCount" ||
        key === "policyEffectiveDate" ||
        key === "policyEffectiveTime" ||
        key === "policyTerminationDate" ||
        key === "policyTerminationTime" ||
        key === "policyTermIndicator" ||
        key === "rateMethod" ||
        key === "regularEmergencyProgramIndicator" ||
        key === "totalBuildingInsuranceCoverage" ||
        key === "totalContentsInsuranceCoverage" ||
        key === "totalInsurancePremiumOfThePolicy"
      ) {
        if (floodpolicyupdatefirst1 == true) {
          restoreStringFloodPolicy += key + "=" + `'${value}'`;
          floodpolicyupdatefirst1 = false;
        } else {
          restoreStringFloodPolicy += "," + key + "=" + `'${value}'`;
        }
      }
    } //end for loop

    //here is where we perform the update queries

    //first start with querying stringUpdateFinalOwner, if it fails we just do nothing
    setTimeout(() => {
      console.log("timeout before update");
    }, 4000);

    //updating owner information as needed
    if (ownerupdatefirst == false) {
      con.query(stringUpdateFinalOwner, function (err, resullt) {
        if (err) {
          //if error in owner then need to back it up using backupstring
          console.log(err);
          con.query(restoreStringOwner, function (err, result) {
            if (err) {
              console.log(err); //just console log error and do nothing
            }
          });
        } //else owner went through, no issues here
      });
    }

    //building side update if needed
    if (buildingupdatefirst == false) {
      con.query(stringUpdateFinalBuilding, function (err, resullt) {
        if (err) {
          console.log(err);
          //if error in owner then need to back it up using backupstring
          con.query(restoreStringBuilding, function (err, result) {
            if (err) {
              console.log(err); //just console log error and do nothing
            }
          });
        } //else owner went through, no issues here
      });
    }

    //location side update if neeeded
    if (locationupdatefirst == false) {
      con.query(stringUdateFinalLocation, function (err, resullt) {
        if (err) {
          console.log(err);
          //if error in owner then need to back it up using backupstring
          con.query(restoreStringLocation, function (err, result) {
            if (err) {
              console.log(err); //just console log error and do nothing
            }
          });
        } //else owner went through, no issues here
      });
    }

    //flood information side update
    if (floodinformationupdatefirst == false) {
      con.query(stringUpdateFinalFloodInformation, function (err, resullt) {
        if (err) {
          //if error in owner then need to back it up using backupstring
          con.query(restoreStringFloodInformation, function (err, result) {
            if (err) {
              console.log(err); //just console log error and do nothing
            }
          });
        } //else owner went through, no issues here
      });
    }

    if (floodpolicyupdatefirst == false) {
      con.query(stringUpdateFinalFloodPolicy, function (err, resullt) {
        if (err) {
          console.log(err);
          //if error in owner then need to back it up using backupstring
          con.query(restoreStringFloodPolicy, function (err, result) {
            if (err) {
              console.log(err); //just console log error and do nothing
            }
          });
        } //else owner went through, no issues here
      });
    }
    //After a successful update the code returns the user to the user selection screen
    res.redirect("/");
  });
};
