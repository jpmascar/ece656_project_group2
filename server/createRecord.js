import { con } from "../app.js";

export const createRecord = function (req, res) {
  //retrieve data as a JSON from UI form to create a record
  let data = req.body;

  //basic concept INSERT INTO (specific table) (all columns witin table) VALUES (values to match given form submitted by user)

  let stringOwnerName = "INSERT INTO Owner (policyID,";
  let stringOwnerValues = "VALUES (";

  let stringBuildingtableName = "INSERT INTO Building (policyID,";
  let stringBuildingtableValues = "VALUES (";

  let stringLocationName = "INSERT INTO Location (policyID,";
  let stringLocationValues = "VALUES (";

  let stringFloodInformationName = "INSERT INTO FloodLocation (policyID,";
  let stringFloodInformationValues = "VALUES (";

  let stringFloodPolicyName = "INSERT INTO FloodPolicy (policyID,";
  let stringFloodPolicyValues = "VALUES (";

  //add random string of 24 characters to policy ID

  let randomPolicyID = makeid(24);

  stringOwnerValues += `'${randomPolicyID}'` + ",";
  stringBuildingtableValues += `'${randomPolicyID}'` + ",";
  stringLocationValues += `'${randomPolicyID}'` + ",";
  stringFloodInformationValues += `'${randomPolicyID}'` + ",";
  stringFloodPolicyValues += `'${randomPolicyID}'` + ",";

  let Owner = true;
  let Building = true;
  let Location = true;
  let FloodInformation = true;
  let FloodPolicy = true;

  //This part of the code goes through every submitted entry of the form to create the appropiate query string
  //for each table
  for (const [key, value] of Object.entries(data)) {
    //This is for Owner string
    if (
      key === "primaryResidenceIndicator" ||
      key === "smallBusinessIndicatorBuilding" ||
      key === "nonProfitIndicator"
    ) {
      if (Owner == true) {
        stringOwnerName += key;
        stringOwnerValues += "NULLIF(" + `'${value}'` + ",'')";
        Owner = false;
      } else {
        stringOwnerName += ", " + key;
        stringOwnerValues += ", " + "NULLIF(" + `'${value}'` + ",'')";
      }
    }
    //this is for Building string
    if (
      key === "agricultureStructureIndicator" ||
      key === "basementEnclosureCrawlspace" ||
      key === "condominiumIndicator" ||
      key === "construction" ||
      key === "elevationBuildingIndicator" ||
      key === "elevationDifference" ||
      key === "locationOfContents" ||
      key === "lowestAdjacentGrade" ||
      key === "lowestFloorElevation" ||
      key === "numberOfFloorsInTheInsuredBuilding" ||
      key === "obstructionType" ||
      key === "occupancyType" ||
      key === "postFIRMConstructionIndicator" ||
      key === "originalConstructionDate" ||
      key === "originalConstructionTime"
    ) {
      if (Building == true) {
        stringBuildingtableName += key;
        stringBuildingtableValues += "NULLIF(" + `'${value}'` + ",'')";
        Building = false;
      } else {
        stringBuildingtableName += ", " + key;
        stringBuildingtableValues += ", " + "NULLIF(" + `'${value}'` + ",'')";
      }
    }

    //this is for location string
    if (
      key === "censusTract" ||
      key === "countyCode" ||
      key === "latitude" ||
      key === "longitude" ||
      key === "propertyState" ||
      key === "reportedZipCode" ||
      key === "reportedCity"
    ) {
      if (Location == true) {
        stringLocationName += key;
        stringLocationValues += "NULLIF(" + `'${value}'` + ",'')";
        Location = false;
      } else {
        stringLocationName += ", " + key;
        stringLocationValues += ", " + "NULLIF(" + `'${value}'` + ",'')";
      }
    }

    // this is for flood information string
    if (
      key === "policyID" ||
      key === "baseFloodElevation" ||
      key === "floodZone"
    ) {
      if (FloodInformation == true) {
        stringFloodInformationName += key;
        stringFloodInformationValues += "NULLIF(" + `'${value}'` + ",'')";
        FloodInformation = false;
      } else {
        stringFloodInformationName += ", " + key;
        stringFloodInformationValues +=
          ", " + "NULLIF(" + `'${value}'` + ",'')";
      }
    }

    //this is for flood policy string

    if (
      key === "policyID" ||
      key === "cancellationDate" ||
      key === "cancellationTime" ||
      key === "crsClassCode" ||
      key === "deductibleAmountInBuildingCoverage" ||
      key === "deductibleAmountInContentsCoverage" ||
      key === "elevationCertificateIndicator" ||
      key === "federalPolicyFee" ||
      key === "hfiaaSurcharge" ||
      key === "houseOfWorshipIndicator" ||
      key === "originalNBDate" ||
      key === "originalNBTime" ||
      key === "policyCost" ||
      key === "policyCount" ||
      key === "policyEffectiveDate" ||
      key === "policyEffectiveTime" ||
      key === "policyTerminationDate" ||
      key === "policyTerminationTime" ||
      key === "policyTermIndicator" ||
      key === "rateMethod" ||
      key === "regularEmergencyProgramIndicator" ||
      key === "totalBuildingInsuranceCoverage" ||
      key === "totalContentsInsuranceCoverage" ||
      key === "totalInsurancePremiumOfThePolicy"
    ) {
      if (FloodPolicy == true) {
        stringFloodPolicyName += key;
        stringFloodPolicyValues += "NULLIF(" + `'${value}'` + ",'')";
        FloodPolicy = false;
      } else {
        stringFloodPolicyName += ", " + key;
        stringFloodPolicyValues += ", " + "NULLIF(" + `'${value}'` + ",'')";
      }
    }
  } //end for

  // adding final closing brackets to all strings and concatinating all of them respectively
  stringOwnerName += ")" + " " + stringOwnerValues + ")";
  stringBuildingtableName += ")" + " " + stringBuildingtableValues + ")";
  stringLocationName += ")" + " " + stringLocationValues + ")";
  stringFloodInformationName += ")" + " " + stringFloodInformationValues + ")";
  stringFloodPolicyName += ")" + " " + stringFloodPolicyValues + ")";

  let deleteStringBuilding =
    "DELETE FROM Building WHERE policyID = " + `'${randomPolicyID}'`;
  let deleteStringLocation =
    "DELETE FROM Location WHERE policyID = " + `'${randomPolicyID}'`;
  let deleteStringFloodInformation =
    "DELETE FROM FloodLocation  WHERE policyID = " + `'${randomPolicyID}'`;
  let deleteStringFloodPolicy =
    "DELETE FROM FloodPolicy WHERE policyID = " + `'${randomPolicyID}'`;

  con.query(stringFloodPolicyName, function (err, result, fields) {
    //beginning of first query
    if (err) {
      console.log("Error 1 at Floodpolicy");
      console.log(err);
      res.redirect("/");
    } else {
      let resultArray = Object.values(JSON.parse(JSON.stringify(result))); //flood policy went throug, nneed to do Location next
      // setTimeout(() => {
      //   console.log(resultArray);
      // }, 4000);
      con.query(stringLocationName, function (err, result, fields) {
        if (err) {
          console.log("error 2 at Floodpolicy");
          console.log(err);
          con.query(deleteStringFloodPolicy, function (err, result, fields) {
            if (err) {
              console.log("error deleting flood policy at location");
              console.log(err);
            }
          });
          res.redirect("/");
        } else {
          let resultArray = Object.values(JSON.parse(JSON.stringify(result)));
          // setTimeout(() => {
          //   console.log(resultArray);
          // }, 4000);

          con.query(stringFloodInformationName, function (err, result, fields) {
            //Location and flood policy went through, now need to do Floodinformation
            if (err) {
              console.log("error in floodinformation");
              console.log(err);
              con.query(deleteStringLocation, function (err, result, fields) {
                //floodinfo did not go through thus delete floodpolicy,location
                if (err) {
                  console.log("error deleting floodpolicy at floodinformation");
                  console.log(err);
                }
              });

              con.query(
                deleteStringFloodPolicy,
                function (err, result, fields) {
                  if (err) {
                    console.log("error deleting location at floodinformation");
                    console.log(err);
                  }
                }
              );
              res.redirect("/");
            } else {
              let resultArray = Object.values(
                JSON.parse(JSON.stringify(result))
              );
              // setTimeout(() => {
              //   console.log(resultArray);
              // }, 4000);
              con.query(
                stringBuildingtableName,
                function (err, result, fields) {
                  if (err) {
                    //building didn't go through, thus delete floodlocation, floodpolicy and location
                    console.log("error in building");
                    console.log(err);
                    con.query(
                      deleteStringFloodInformation,
                      function (err, result, fields) {
                        if (err) {
                          console.log(err);
                        }
                      }
                    );

                    con.query(
                      deleteStringLocation,
                      function (err, result, fields) {
                        if (err) {
                          console.log(err);
                        }
                      }
                    );

                    con.query(
                      deleteStringFloodPolicy,
                      function (err, result, fields) {
                        if (err) {
                          console.log(err);
                        }
                      }
                    );
                    res.redirect("/");
                  } else {
                    //buidling went through, thhus need to do owner last
                    let resultArray = Object.values(
                      JSON.parse(JSON.stringify(result))
                    );
                    setTimeout(() => {
                      console.log(resultArray);
                    }, 14000);
                    //put a timeout of  seconds
                    con.query(stringOwnerName, function (err, result, fields) {
                      if (err) {
                        //owner didn't go through thus need to delete everything else
                        console.log("error in string Owner creation");
                        console.log(err);
                        con.query(
                          deleteStringBuilding,
                          function (err, result, fields) {
                            if (err) {
                              console.log(err);
                            }
                          }
                        );

                        con.query(
                          deleteStringFloodInformation,
                          function (err, result, fields) {
                            if (err) {
                              console.log(err);
                            }
                          }
                        );

                        con.query(
                          deleteStringLocation,
                          function (err, result, fields) {
                            if (err) {
                              console.log(err);
                            }
                          }
                        );

                        con.query(
                          deleteStringFloodPolicy,
                          function (err, result, fields) {
                            if (err) {
                              console.log(err);
                              res.redirect("/");
                            }
                          }
                        );
                      } else {
                        let resultArray = Object.values(
                          JSON.parse(JSON.stringify(result))
                        );
                        console.log(resultArray);
                        res.redirect(`/recordconfirmation/${randomPolicyID}`);
                      }
                    });
                  }
                }
              );
            }
          });
        }
      });
    }
  });
};

function makeid(length) {
  var result = "";
  var characters =
    "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
  var charactersLength = characters.length;
  for (var i = 0; i < length; i++) {
    result += characters.charAt(Math.floor(Math.random() * charactersLength));
  }
  return result;
}
