import { con } from "../app.js";

export const deleteRecord = function (req, res) {
  let deleteRecordPolicyID = req.params.id;
  let deleteOwner =
    "DELETE FROM Owner WHERE policyID = " + `'${deleteRecordPolicyID}'`;
  let deleteBuilding =
    "DELETE FROM Building WHERE policyID = " + `'${deleteRecordPolicyID}'`;
  let deleteLocation =
    "DELETE FROM Location WHERE policyID = " + `'${deleteRecordPolicyID}'`;
  let deleteFloodInformation =
    "DELETE FROM FloodLocation WHERE policyID = " + `'${deleteRecordPolicyID}'`;
  let deleteFloodPolicy =
    "DELETE FROM FloodPolicy WHERE policyID = " + `'${deleteRecordPolicyID}'`;

  con.query(deleteOwner, function (err, result) {
    if (err) {
      console.log(err); //just console log error and do nothing
    } else {
      con.query(deleteBuilding, function (err, result) {
        if (err) {
          console.log(err); //just console log error and do nothing
        } else {
          con.query(deleteFloodInformation, function (err, result) {
            if (err) {
              console.log(err); //just console log error and do nothing
            } else {
              con.query(deleteLocation, function (err, result) {
                if (err) {
                  console.log(err); //just console log error and do nothing
                } else {
                  con.query(deleteFloodPolicy, function (err, result) {
                    if (err) {
                      console.log(err); //just console log error and do nothing
                    } else {
                      res.render("deleterecord.pug", {
                        recordPolicyID: deleteRecordPolicyID,
                      });
                    }
                  });
                }
              });
            }
          });
        }
      });
    }
  });
};
