import express from 'express'
import { getEstimate } from './getEstimate.js';
import { createRecord } from './createRecord.js'
import { readRecord } from './readRecord.js'
import { deleteRecord } from './deleteRecord.js'
import { updateRecord } from './updateRecord.js';

const router = express.Router();

router.route('/getEstimate').post(getEstimate);

router.route('/createRecord').post(createRecord);

router.route('/viewrecord').post(readRecord)

router.route('/deleterecord/:id').get(deleteRecord);

router.route("/updaterecord/:id").post(updateRecord);
  

export default router;