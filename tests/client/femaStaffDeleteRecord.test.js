//this test is for the verfication of a deletion of a given policy ID in the database

//this test script is desinged to run a full test of view record to ensure view recorrd is outputting correct values
//wit accordance to a given policy ID

const puppeteer = require("puppeteer");
// import {puppeteer} from 'puppeteer'
const delay = require("delay");

test("Validates the fema staff delete record page", async () => {
  const browser = await puppeteer.launch({
    headless: false,
    slowMo: 20,
    ignoreHTTPSErrors: true,
    args: ["--window-size=1440,900"], // new option
  });

  const page = await browser.newPage();
  await page.setViewport({
    width: 1440,
    height: 900,
  });

  await page.goto("http://localhost:3000");
  await delay(6000);
  await page.click("[id = femaStaff]");
  await delay(2500);
  await page.click("[id = viewEditDelete]");
  await delay(2500);
  await page.click("input#policyID");
  await page.type("input#policyID", "iCXG42OzvjWvZp8UoRe0YNd2"); //NOTE NEED TO CHANGE ID HERE TO FIT TEST SCRIPT
  await delay(2500);
  await page.click("[id = submitPolicyID]");
  await delay(2500);
  await page.click("[id = deleteRecord]");
  await browser.close();
}); //end of test script
jest.setTimeout(500000);
