//this file contains the code necessary to perform a system level test on the given owner page

const puppeteer = require("puppeteer");
// import {puppeteer} from 'puppeteer'
const delay = require("delay");

test("Validates the Owner side of getting an estimate", async () => {
  const browser = await puppeteer.launch({
    headless: false,
    slowMo: 20,
    ignoreHTTPSErrors: true,
    args: ["--window-size=1440,900"], // new option
  });

  const page = await browser.newPage();
  await page.setViewport({
    width: 1440,
    height: 900,
  });

  await page.goto("http://localhost:3000");
  await delay(6000);
  await page.click("[id = femaStaff]");
  await delay(2500);
  await page.click("[id = createRecord]");
  await delay(2500);
  await page.select("select#PrimaryResidenceIndicator", "1");
  await delay(2500);
  await page.select("select#smallBusinessIndicatorBuilding", "0");
  await delay(2500);
  await page.select("select#nonProfitIndicator", "0");
  await delay(2500);
  await page.select("select#numberOfFloorsInTheInsuredBuilding", "1");

  await delay(2500);

  await page.select("select#AgricultureStructureIndicator", "1");

  await delay(2500);

  await page.select("select#basementEnclosureCrawlspace", "1");

  await delay(2500);

  await page.select("select#condominiumIndicator", "A");

  await delay(2500);

  await page.select("select#construction", "0");

  await delay(2500);

  await page.select("select#elevationBuildingIndicator", "1");
  await delay(2500);

  await page.click("input#ElevationDifference");
  await page.type("input#ElevationDifference", "112");
  await delay(2500);
  await page.select("select#locationOfContents", "4");

  await delay(2500);
  await page.click("input#lowestAdjacentGrade");
  await page.type("input#lowestAdjacentGrade", "50");
  await delay(2500);

  //need to fix how to select dates
  await page.click("input#originalConstructionDate");
  await page.$eval(
    "input#originalConstructionDate",
    (el) => (el.value = "1997-02-10")
  );
  delay(2500);
  await page.click("input#originalConstructionTime");
  await page.$eval(
    "input#originalConstructionTime",
    (el) => (el.value = "12:30")
  );
  //delay(2500)
  await page.click("input#censusTract");
  await page.type("input#censusTract", "123456");
  //delay(2500)

  await page.click("input#countyCode");
  await page.type("input#countyCode", "12345");
  delay(2500);
  await page.click("input#latitude");
  await page.type("input#latitude", "12.9");
  delay(2500);

  await page.click("input#longitude");
  await page.type("input#longitude", "-38.2");
  delay(2500);
  await page.select("select#propertyState", "TX");

  delay(2500);
  await page.click("input#reportedZipCode");
  await page.type("input#reportedZipCode", "55672");
  delay(2500);
  await page.click("input#reportedCity");
  await page.type("input#reportedCity", "Compton");

  delay(2500);
  await page.click("input#BaseFloodElevation");
  await page.type("input#BaseFloodElevation", "50");
  delay(2500);

  await page.click("input#floodZone");
  await page.type("input#floodZone", "A");

  delay(2500);

  await page.click("input#CancellationDate");
  await page.$eval("input#CancellationDate", (el) => (el.value = "2000-01-01"));
  delay(2500);
  await page.click("input#CancellationTime");
  await page.$eval("input#CancellationTime", (el) => (el.value = "11:12"));
  delay(2500);
  await page.click("select#crsClassCode");
  await page.select("select#crsClassCode", "5");
  delay(2500);
  await page.click("select#deductibleAmountInBuildingCoverage");
  await page.select("select#deductibleAmountInBuildingCoverage", "A");
  delay(2500);
  await page.click("select#deductibleAmountInContentsCoverage");
  await page.select("select#deductibleAmountInContentsCoverage", "G");
  delay(2500);
  await page.click("select#elevationCertificateIndicator");
  await page.select("select#elevationCertificateIndicator", "2");
  delay(2500);
  await page.click("input#federalPolicyFee");
  await page.type("input#federalPolicyFee", "500");
  delay(2500);
  await page.click("input#hfiaaSurcharge");
  await page.type("input#hfiaaSurcharge", "25");
  delay(2500);
  await page.click("select#houseOfWorshipIndicator");
  await page.select("select#houseOfWorshipIndicator", "1");
  delay(2500);
  await page.click("input#originalNBDate");
  await page.$eval("input#originalNBDate", (el) => (el.value = "2021-01-01"));
  delay(2500);
  await page.click("input#originalNBTime");
  await page.$eval("input#originalNBTime", (el) => (el.value = "09:12"));
  delay(2500);
  await page.click("input#policyCost");
  await page.type("input#policyCost", "5000");
  delay(2500);
  await page.click("input#policyCount");
  await page.type("input#policyCount", "4");

  await page.click("input#policyEffectiveDate");
  await page.$eval(
    "input#policyEffectiveDate",
    (el) => (el.value = "2015-02-10")
  );
  await page.click("input#policyEffectiveTime");
  await page.$eval("input#policyEffectiveTime", (el) => (el.value = "12:30"));
  delay(2500);
  await page.click("input#policyTerminationDate");
  await page.$eval(
    "input#policyTerminationDate",
    (el) => (el.value = "2016-02-10")
  );
  await page.click("input#policyTerminationTime");
  await page.$eval("input#policyTerminationTime", (el) => (el.value = "12:30"));
  delay(2500);
  await page.click("select#rateMethod");
  await page.select("select#rateMethod", "3");
  delay(2500);
  await page.click("select#regularEmergencyProgramIndicator");
  await page.select("select#regularEmergencyProgramIndicator", "R");
  delay(2500);

  await page.click("input#totalBuildingInsuranceCoverage");
  await page.type("input#totalBuildingInsuranceCoverage", "40000");
  delay(2500);

  await page.click("input#totalContentsInsuranceCoverage");
  await page.type("input#totalContentsInsuranceCoverage", "20000");
  delay(2500);
  await page.click("input#totalInsurancePremiumOfThePolicy");
  await page.type("input#totalInsurancePremiumOfThePolicy", "64200");
  delay(2500);
  await page.click("select#policyTermIndicator");
  await page.select("select#policyTermIndicator", "1");
  delay(2500);
  await page.click("[id = submitCreateRecord]");
  await delay(5000);

  //closing browser
  await browser.close();
});
jest.setTimeout(500000);
