//this file contains the code necessary to perform a system level test on the given owner page

const puppeteer = require('puppeteer')
// import {puppeteer} from 'puppeteer'
const delay = require('delay')

test('Validates the Owner side of getting an estimate', async() => {
    const browser = await puppeteer.launch({
        headless: false,
        slowMo: 20,
        ignoreHTTPSErrors: true,
        args: ['--window-size=1440,900'] // new option

    });

    const page = await browser.newPage();
    await page.setViewport({
        width: 1440,
        height: 900
    });

    await page.goto('http://localhost:3000')
    await delay(6000)
    await page.click( '[id = buildingOwner]')
    await delay(2500)
    await page.select('select#primaryResidenceIndicator', '1')
    await delay(2500)
    await page.select('select#smallBusinessIndicatorBuilding', '0')
    await delay(2500)
    await page.select('select#nonProfitIndicator' , '0')
    await delay(2500)

    await page.select('select#propertyState', 'TX')

    await delay(2500)

    await page.click('[id = submitEstimate]')
    await delay(10000)

    await browser.close()

})
jest.setTimeout(500000);