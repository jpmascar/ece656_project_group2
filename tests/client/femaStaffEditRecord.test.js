//this test script was designed to edit a given record in the client and test if the functionality is correct

const puppeteer = require("puppeteer");
// import {puppeteer} from 'puppeteer'
const delay = require("delay");

test("Validates fema staff edit page", async () => {
  const browser = await puppeteer.launch({
    headless: false,
    slowMo: 20,
    ignoreHTTPSErrors: true,
    args: ["--window-size=1440,900"], // new option
  });

  const page = await browser.newPage();
  await page.setViewport({
    width: 1440,
    height: 900,
  });

  await page.goto("http://localhost:3000/searchid");
  await delay(6000);
  await page.click("input#policyID");
  await page.type("input#policyID", "25lG2awIdJcKvKwzuoAunaQf"); //NOTE NEED TO CHANGE ID HERE TO FIT TEST SCRIPT
  await delay(2500);
  await page.click("[id = submitPolicyID]");

  await delay(10000);
  await page.click("[id = editRecord]");

  await page.select("select#PrimaryResidenceIndicator", "0");
  await delay(2500);
  await page.select("select#SmallBusinessIndicatorBuilding", "1");
  await delay(2500);
  await page.select("select#nonProfitIndicator", "1");
  await delay(2500);
  await page.select("select#numberOfFloorsInTheInsuredBuilding", "3");

  await delay(2500);

  await page.select("select#AgricultureStructureIndicator", "0");

  await delay(2500);

  await page.select("select#basementEnclosureCrawlspace", "2");

  await delay(2500);

  await page.select("select#condominiumIndicator", "U");

  await delay(2500);

  await page.select("select#construction", "0");

  await delay(2500);

  await page.select("select#elevationBuildingIndicator", "1");
  await delay(2500);

  await page.click("input#ElevationDifference");
  await page.type("input#ElevationDifference", "60");
  await delay(2500);
  await page.select("select#locationOfContents", "3");

  await delay(2500);
  await page.click("input#lowestAdjacentGrade");
  await page.type("input#lowestAdjacentGrade", "60");
  await delay(2500);

  //need to fix how to select dates
  await page.click("input#originalConstructionDate");
  await page.$eval(
    "input#originalConstructionDate",
    (el) => (el.value = "2002-09-10")
  );
  await delay(2500);
  await page.click("input#originalConstructionTime");
  await page.$eval(
    "input#originalConstructionTime",
    (el) => (el.value = "07:30")
  );
  //delay(2500)
  await page.click("input#censusTract");
  await page.type("input#censusTract", "456421");
  //delay(2500)

  await page.click("input#countyCode");
  await page.type("input#countyCode", "56780");
  await delay(2500);
  await page.click("input#latitude");
  await page.type("input#latitude", "12.9");
  await delay(2500);

  await page.click("input#longitude");
  await page.type("input#longitude", "-138.2");
  await delay(2500);
  await page.select("select#propertyState", "LA");

  delay(2500);
  await page.click("input#reportedZipCode");
  await page.type("input#reportedZipCode", "55672");
  delay(2500);
  await page.click("input#reportedCity");
  await page.type("input#reportedCity", "Denmark");

  await delay(2500);
  await page.click("input#BaseFloodElevation");
  await page.type("input#BaseFloodElevation", "70");
  delay(2500);

  await page.click("input#floodZone");
  await page.type("input#floodZone", "V");

  delay(2500);

  await page.click("input#CancellationDate");
  await page.$eval("input#CancellationDate", (el) => (el.value = "2017-01-01"));
  delay(2500);
  await page.click("input#CancellationTime");
  await page.$eval("input#CancellationTime", (el) => (el.value = "15:00"));
  delay(2500);
  await page.click("select#crsClassCode");
  await page.select("select#crsClassCode", "7");
  delay(2500);
  await page.click("select#deductibleAmountInBuildingCoverage");
  await page.select("select#deductibleAmountInBuildingCoverage", "A");
  delay(2500);
  await page.click("select#deductibleAmountInContentsCoverage");
  await page.select("select#deductibleAmountInContentsCoverage", "G");
  delay(2500);
  await page.click("select#elevationCertificateIndicator");
  await page.select("select#elevationCertificateIndicator", "2");
  delay(2500);
  await page.click("input#federalPolicyFee");
  await page.type("input#federalPolicyFee", "500");
  delay(2500);
  await page.click("input#hfiaaSurcharge");
  await page.type("input#hfiaaSurcharge", "25");
  delay(2500);
  await page.click("select#houseOfWorshipIndicator");
  await page.select("select#houseOfWorshipIndicator", "1");
  delay(2500);
  await page.click("input#originalNBDate");
  await page.$eval("input#originalNBDate", (el) => (el.value = "2021-01-01"));
  delay(2500);
  await page.click("input#originalNBTime");
  await page.$eval("input#originalNBTime", (el) => (el.value = "09:12"));
  delay(2500);
  await page.click("input#policyCost");
  await page.type("input#policyCost", "5000");
  delay(2500);
  await page.click("input#policyCount");
  await page.type("input#policyCount", "4");
  delay(2500);
  await page.click("select#rateMethod");
  await page.select("select#rateMethod", "3");
  delay(2500);
  await page.click("select#regularEmergencyProgramIndicator");
  await page.select("select#regularEmergencyProgramIndicator", "R");
  delay(2500);

  await page.click("input#totalBuildingInsuranceCoverage");
  await page.type("input#totalBuildingInsuranceCoverage", "40000");
  delay(2500);

  await page.click("input#totalContentsInsuranceCoverage");
  await page.type("input#totalContentsInsuranceCoverage", "20000");
  delay(2500);
  await page.click("input#totalInsurancePremiumOfThePolicy");
  await page.type("input#totalInsurancePremiumOfThePolicy", "64200");
  delay(2500);
  await page.click("select#policyTermIndicator");
  await page.select("select#policyTermIndicator", "1");
  delay(2500);
  await page.click("[id = submitUpdate]");
  await delay(6000);
  await page.goto("http://localhost:3000/searchid");
  await delay(6000);
  await page.click("input#policyID");
  await page.type("input#policyID", "25lG2awIdJcKvKwzuoAunaQf"); //NOTE NEED TO CHANGE ID HERE TO FIT TEST SCRIPT
  await delay(2500);
  await page.click("[id = submitPolicyID]");

  await delay(10000);

  await browser.close();
}); //end of test script
jest.setTimeout(500000);
