import mysql from "mysql";
import express from "express";
import path from "path";
import bodyParser from "body-parser";
import routes from "./server/routes.js";

//Initialize express application
const app = express();

//Set body parser for JSON communication with the server
app.use(bodyParser.urlencoded({ extended: true }));

//Load view engine to render server side user interfaces with pug
app.set(
  "views",
  path.join("/home/user/Desktop/ECE656/ece656_project_group2" + "/views")
);
app.set("view engine", "pug");
app.use(
  express.static("/home/user/Desktop/ECE656/ece656_project_group2" + "/public")
);
app.use(
  express.static(
    "/home/user/Desktop/ECE656/ece656_project_group2" +
      "/node_modules/jquery/dist"
  )
);

//Create database connection with configuration
export var con = mysql.createConnection({
  host: "localhost",
  user: "root",
  password: "Kaviuser123!",
  database: "ece656",
  dateStrings: ["DATE"],
});

//Connect to the database
con.connect(function (err) {
  if (err) throw err;
  console.log("Connected!");
});

//---------------------------------------------------------------------------------------------------
//Application endpoints below:
app.get("/", function (req, res) {
  res.render("userselection.pug");
});

app.get("/owner", function (req, res) {
  res.render("owner.pug");
});

app.post("/getEstimate", routes);

app.post("/createRecord", routes);

app.get("/owner", function (req, res) {
  res.render("owner.pug");
});

app.get("/femastaff", function (req, res) {
  res.render("femastaff.pug");
});

app.get("/createrecord", function (req, res) {
  res.render("createrecord.pug");
});

app.get("/updaterecord", function (req, res) {
  res.render("updaterecord.pug");
});

app.get("/recordconfirmation/:id", function (req, res) {
  res.render("recordconfirmation.pug", { policyID: req.params.id });
});

app.get("/searchid", function (req, res) {
  res.render("searchid.pug");
});

app.post("/viewrecord", routes);

app.get("/editrecord/:id", function (req, res) {
  let recordPolicyID = req.params.id;
  res.render("editrecord.pug", { recordPolicyID: recordPolicyID });
});

app.get("/deleterecord/:id", routes);

app.post("/updaterecord/:id", routes);

//-----------------------------------------------------------------------------

//This part of the code launches the server on localhost port 3000
app.listen(3000, function () {
  console.log("Node server started on localhost port 3000");
});
